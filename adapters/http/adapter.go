package http

import (
	"gitlab.com/zenport.io/go-assignment/engine"
	"fmt"
)

type HTTPAdapter struct{
	handler RouteHandler
}

func (adapter *HTTPAdapter) Start() {
	// todo: start to listen
	fmt.Println("listening : 8000") // temporary message
	adapter.handler.Start()
}

func (adapter *HTTPAdapter) Stop() {
	// todo: shutdown server
	adapter.handler.Stop()
}

func NewHTTPAdapter(e engine.Engine) *HTTPAdapter {
	// todo: init your http server and routes
	adapter := HTTPAdapter{}
	adapter.handler = CreateRouteHandler(e.GetKnightRepository())
	return &adapter
}
