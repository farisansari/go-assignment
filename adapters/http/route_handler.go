package http

import (
	"net/http"
	"time"
	"log"
	"encoding/json"
	"io/ioutil"
	"strconv"
	"errors"
	"github.com/gorilla/mux"
	"gitlab.com/zenport.io/go-assignment/domain"
	"gitlab.com/zenport.io/go-assignment/engine"
)

func CreateRouteHandler(knightRepository engine.KnightRepository) *routeHandler{
	var rhandler routeHandler
	rhandler.running = false
	rhandler.InitHttpServer(knightRepository)
	return &rhandler
}

func keyExists(obj map[string]interface{}, key string) bool{
	_, found := obj[key]
	if found{
		return true
	}
	return false
}
type RouteHandler interface{
	Start()
	Stop()
}

type Route struct {
    Name        string
    Method      string
    Pattern     string
    HandlerFunc http.HandlerFunc
}

type routeHandler struct {
	routes []Route
	server *http.Server
	running  bool
	knightRepository engine.KnightRepository
}

func (handler routeHandler)httpCreateKnight(resW http.ResponseWriter, req *http.Request){
	req.ParseForm()
	
	resW.Header().Add("Content-Type", "application/json")
	body, err := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	jsonMap := make(map[string]interface{})
	err = json.Unmarshal(body, &jsonMap)

	if (keyExists(jsonMap,"name")==false || 
			keyExists(jsonMap,"strength") == false ||
			keyExists(jsonMap,"weapon_power")==false){

		err = errors.New("Bad Data received to create Knight.")
		checkError(err, resW, "", http.StatusBadRequest)
		return
	} 

	name := jsonMap["name"].(string)
	id := "0"
	strength := jsonMap["strength"].(float64)
	if(checkError(err, resW, "Bad Data received to create Knight.", http.StatusBadRequest)){
		return
	}
	weaponPower := jsonMap["weapon_power"].(float64)
	if(checkError(err, resW, "Bad Data received to create Knight.", http.StatusBadRequest)){
		return
	}
	
	knight := domain.Knight {id, name, strength, weaponPower}
	if(checkError(err, resW, "Bad Data received to create Knight.",http.StatusBadRequest)){
		return
	}
	err = handler.knightRepository.Save(&knight)
	if checkError(err, resW, "Failed to add Knight to Database.", http.StatusBadRequest){
				return
	}
	PostDataWithStatus(nil, resW, http.StatusCreated)
}


func (handler routeHandler)httpGetAllKnights(resW http.ResponseWriter, req *http.Request){
	resW.Header().Add("Content-Type", "application/json")
	// get all knights
	knights, err := handler.knightRepository.FindAll()
	if(err != nil){
		PostDataWithStatus(nil, resW, http.StatusOK)
	}
	data, err := json.Marshal(knights)
	if checkError(err, resW, "Failed to get All Knights.", http.StatusBadRequest){
		return
	}
	resW.Write(data)
}

func (handler routeHandler)httpGetKnight(resW http.ResponseWriter, req *http.Request){
	id := mux.Vars(req)["id"]
	resW.Header().Add("Content-Type", "application/json")
	msg := "Knight #" + id + " not found."
	knight, err := handler.knightRepository.Find(id)
	if(err != nil){
		checkError(err, resW, msg, http.StatusNotFound)
	}else{
		data, err := json.Marshal(knight)
		if checkError(err, resW, msg, http.StatusNotFound){
			return
		}
		PostDataWithStatus(data, resW, http.StatusOK)
	}
}

func (handler routeHandler)NewRouter() *mux.Router {
    router := mux.NewRouter().StrictSlash(true)
    handler.routes = []Route{
	    Route{
	        "getKnight",
	        "GET",
	        "/knight/{id}",
	        handler.httpGetKnight,
	    },
	    Route{
	        "getAllKnights",
	        "GET",
	        "/knight",
	        handler.httpGetAllKnights,
	    },
	    Route{
	        "createKnight",
	        "POST",
	        "/knight",
	        handler.httpCreateKnight,
	    },
	}

    for _, route := range handler.routes {
        router.
            Methods(route.Method).
            Path(route.Pattern).
            Name(route.Name).
            Handler(route.HandlerFunc)
    }

    return router
}
func (handler *routeHandler)InitHttpServer(knightRepository engine.KnightRepository) {
    if handler.running == true {
    	return
    }
    handler.knightRepository = knightRepository
    router := handler.NewRouter()

	handler.server = &http.Server{
		Handler:      router,
		Addr:         ":8000",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
    
}

func (handler *routeHandler) Start(){
	if handler.running {
    	return
    }
    handler.running = true
	go func() {
		if err := handler.server.ListenAndServe(); err != nil {
            log.Printf("ListenAndServe() error: %s", err)
        }
    }()
}

func (handler *routeHandler) Stop(){
	if handler.running == false {
    	return
    }

	handler.server.Shutdown(nil)
	handler.running = false;
}

// ----------------------------------
func PostDataWithStatus(data []byte, resW http.ResponseWriter, statusCode int){
	resW.WriteHeader(statusCode)
	strData := 
		"{" + 
			"\"code\":\"" + strconv.Itoa(statusCode) + "\"";
	if(data != nil){
			strData += 
			","+
				"\"message\":" + "\"" + string(data) + "\""
	}
	strData += "}"
	resW.Write([]byte(strData))
}

func checkError(err error, resW http.ResponseWriter, msg string, statusCode int) bool{
	if(err != nil){
		resW.WriteHeader(statusCode)
		
		msg:=
			"{" + 
				"\"code\":\"" + strconv.Itoa(http.StatusBadRequest) + "\","+
				"\"message\":" + "\"" + msg +"\"" +
			"}"
		
		//fmt.Println(msg)
		resW.Write([]byte(msg))	
		return true
	}
	return false
}
