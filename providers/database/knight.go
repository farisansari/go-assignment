package database

import (
	"gitlab.com/zenport.io/go-assignment/domain"
	"strconv"
	"errors"
	"database/sql"
	_ "github.com/lib/pq"
)

type knightRepository struct{
	Db *sql.DB
}

func (repository knightRepository) Close() {
	repository.Db.Close()
}

func (repository knightRepository) Find(ID string) (*domain.Knight,error){
	strQuery := `
		SELECT
			id,
			name,
			strength,
			weapon_power
		FROM knights
		WHERE id = ` + "'"+ID+"';"
	rows, err := repository.Db.Query(strQuery)
	if(err != nil){
		//fmt.Println("error: " + err.Error())
		return nil, err
	}
	if(rows == nil){
		return nil, errors.New("No Record in Database!")
	}
	defer rows.Close()
	for rows.Next() {
		knight := domain.Knight{}
		err = rows.Scan(
			&knight.Id,
			&knight.Name,
			&knight.Strength,
			&knight.WeaponPower,
		)
		if err != nil {
			return nil, err
		}
		return &knight, nil
	}
	return nil, errors.New("No Record Matched!")
}

func (repository knightRepository) FindAll() ([]*domain.Knight, error){
	rows, err := repository.Db.Query(`
		SELECT
			id,
			name,
			strength,
			weapon_power
		FROM knights
		ORDER BY name DESC`)
	knights := make([]*domain.Knight, 0, 100)
	if(err != nil){
		//fmt.Println("error: " + err.Error())
		return knights, err
	}
	if(rows == nil){
		//fmt.Println("returning Empty")
		return knights, nil
	}
	defer rows.Close()
	for rows.Next() {
		knight := domain.Knight{}
		err = rows.Scan(
			&knight.Id,
			&knight.Name,
			&knight.Strength,
			&knight.WeaponPower,
		)
		if err != nil {
			return nil, err
		}
		knights = append(knights, &knight)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}
	return knights, nil
}
func (repository knightRepository) Save(knight *domain.Knight)error{
	strQuery := `
		INSERT INTO knights
		(name, strength, weapon_power)
		VALUES(` +
		"'"+ knight.Name +"'," +
		strconv.FormatFloat(knight.Strength, 'E', -1, 64) +"," +
		strconv.FormatFloat(knight.WeaponPower, 'E', -1, 64)+");"
	_, err := repository.Db.Query(strQuery)
	if(err != nil){
		//fmt.Println("err: "+ err.Error())
		return err
	}
	return nil
}

func (repository knightRepository) Reset()error{
	strQuery := "TRUNCATE TABLE knights;"
	_, err := repository.Db.Query(strQuery)
	if(err != nil){
		//fmt.Println("err: "+ err.Error())
		return err
	}
	return nil
}