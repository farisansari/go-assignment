package database

import (
	"gitlab.com/zenport.io/go-assignment/engine"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
)

 const (
 	DB_HOST		= "localhost"
 	DB_PORT		= "5432"
	DB_USER     = "assignment_user"
	DB_PASS		= "password"
	DB_NAME     = "go_assignment"
)

type Provider struct {
	repository knightRepository
}

func (provider Provider) GetKnightRepository() engine.KnightRepository {
	return provider.repository
}

func (provider *Provider) Close() {
	provider.repository.Close()
}

func NewProvider() *Provider {
	repository := knightRepository{}
	dbinfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s", DB_HOST, DB_PORT, DB_USER, DB_PASS, DB_NAME)
	db, err := sql.Open("postgres", dbinfo)
	if(err != nil){
		fmt.Println(err)
	}
	repository.Db = db
	return &Provider{repository}
}
