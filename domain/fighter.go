package domain

type Fighter interface {
	GetID() string
	GetPower() float64
}

type Knight struct {
	Id string `json:"id"`
	Name string `json:"name"`
	Strength float64 `json:"strength"`
	WeaponPower float64 `json:"weapon_power"`
}

func (knight Knight) GetID() string{
	return knight.Id
}

func (knight Knight) GetPower() float64{
	return float64(knight.Strength + knight.WeaponPower)
}


