# Docker File
# THIS FILE IS NOT WORKING

FROM ubuntu:12.04

#ENV GOROOT /usr/bin/go
ENV GOPATH /app/go-assignemnt/

RUN mkdir -p $GOPATH/src/gitlab.com/zenport.io/go-assignment

# Copy the current directory contents into the container at /app
ADD ./ $GOPATH/src/gitlab.com/zenport.io/go-assignment/
RUN ls $GOPATH/src/gitlab.com/zenport.io/go-assignment/

RUN apt-get update
#RUN apt-get install --assume-yes 
RUN apt-get update && apt-get install -y \
  curl \
  golang-go \
  git \
  postgresql \
  postgresql-contrib

# Install dependencies
RUN go get github.com/gorilla/mux
RUN go get github.com/lib/pq

#RUN ["/bin/bash", "-c", "curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh"]
RUN apt-get install --assume-yes postgresql postgresql-contrib

RUN update-rc.d postgresql enable

RUN cat $GOPATH/src/gitlab.com/zenport.io/go-assignment/scripts/create_schema.sh

#RUN /etc/init.d/postgresql start	
RUN update-rc.d postgresql enable
RUN service postgresql start

RUN ["/bin/bash", "-c", "su - postgres -c $GOPATH/src/gitlab.com/zenport.io/go-assignment/scripts/create_schema.sh"]
#	/app/go-assignment/src/gitlab.com/zenport.io/go-assignment/scripts/create_schema.sh



# Set the working directory
WORKDIR $GOPATH/src/gitlab.com/zenport.io/go-assignment/

# Make port 80 available to the world outside this container
EXPOSE 8000

# Run go App
CMD ["go", "run", "main.go"]
