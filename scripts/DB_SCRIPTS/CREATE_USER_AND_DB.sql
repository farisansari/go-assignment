
--DROP OWNED BY assignment_user CASCADE;
--\connect go_assignment assignment_user localhost 5432
--DROP SCHEMA go_assignment;

DROP OWNED BY assignment_user CASCADE;
DROP DATABASE IF EXISTS go_assignment;
DROP USER IF EXISTS assignment_user;

CREATE USER assignment_user WITH PASSWORD 'password' LOGIN CREATEDB ;

--\disconnect go_assignment
CREATE DATABASE go_assignment OWNER assignment_user;
