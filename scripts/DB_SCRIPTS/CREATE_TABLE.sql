--\connect go_assignment assignment_user localhost 5432
CREATE SCHEMA IF NOT EXISTS go_assignment AUTHORIZATION assignment_user;

DROP TABLE IF EXISTS knights;
CREATE TABLE knights 
(
	id SERIAL NOT NULL,
	name character varying(255) NOT NULL,
	strength int NOT NULL,
	weapon_power int NOT NULL,
	CONSTRAINT knights_pkey PRIMARY KEY (name)
);
