package engine

import (
	"errors"
	"fmt"
	"gitlab.com/zenport.io/go-assignment/domain"
)

func (engine *arenaEngine) GetKnight(ID string) (*domain.Knight, error){
	fighter, _ := engine.knightRepository.Find(ID)
	if fighter == nil {
		return nil, errors.New(fmt.Sprintf("fighter with ID '%s' not found!", ID))
	}
	return fighter, nil
}

func (engine *arenaEngine) ListKnights() []*domain.Knight{
	res, _ := engine.knightRepository.FindAll()
	return res
}

func (engine *arenaEngine) Fight(fighter1ID string, fighter2ID string) domain.Fighter{
	fighter1, _ := engine.knightRepository.Find(fighter1ID)
	fighter2, _ := engine.knightRepository.Find(fighter2ID)
	return engine.arena.Fight(fighter1, fighter2)
}

func (eng *arenaEngine)GetKnightRepository() KnightRepository{
	return eng.knightRepository
}

