# Notes

## checkout the git repo
* **export GOPATH=$HOME/repositories/go-assignment**
* **mkdir -p $GOPATH/src/gitlab.com/zenport.io/go-assignment/**
* **git clone https://gitlab.com/farisansari/go-assignment.git $GOPATH/src/gitlab.com/zenport.io/go-assignment/**
* **cd $GOPATH/src/gitlab.com/zenport.io/go-assignment/scripts** # go to scripts folder in go-assignment
* **./install_go_deps** # execute this script to install the dependencies

## Install Dependencies
* **cd $GOPATH/src/gitlab.com/zenport.io/go-assignment/scripts** # go to scripts folder in go-assignment
* **./install_go_deps** # execute this script to install the dependencies

## Database setup
* make sure user has rights to do the operation (such as create schema, table etc..) change the user if it's not postgres in the script/create_schema.sh script
* **cd $GOPATH/src/gitlab.com/zenport.io/go-assignment/scripts** # go to scripts folder in go-assignment
* **./create_schema.sh # execute script** # uses sudo inside, enter password

## RUNNING Tests and Main
	
### Run Tests
* **cd $GOPATH/src/gitlab.com/zenport.io/go-assignment/scripts** # go to scripts folder in go-assignment
* **./run_tests** # run the script to run test cases
	
### RUN main
* **cd $GOPATH/src/gitlab.com/zenport.io/go-assignment/** # go to go-assignment folder
* **go run main.go**

